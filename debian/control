Source: iwyu
Section: devel
Priority: optional
Maintainer: LLVM Packaging Team <pkg-llvm-team@lists.alioth.debian.org>
Uploaders: Sylvestre Ledru <sylvestre@debian.org>
Build-Depends: clang,
               cmake,
               cmark-gfm,
               debhelper-compat (= 13),
               dh-exec,
               help2man,
               libclang-dev,
               llvm-dev,
               python3:any
Standards-Version: 4.6.0
Homepage: https://include-what-you-use.org/
Vcs-Git: https://salsa.debian.org/pkg-llvm-team/iwyu.git
Vcs-Browser: https://salsa.debian.org/pkg-llvm-team/iwyu
Rules-Requires-Root: no

Package: iwyu
Architecture: any
Depends: ${clang:Depends}, ${misc:Depends}, ${shlibs:Depends}
Recommends: python3:any
Description: Analyze #includes in C and C++ source files
 "Include what you use" means this: for every symbol (type, function variable,
 or macro) that you use in foo.cc, either foo.cc or foo.h should #include a .h
 file that exports the declaration of that symbol. The include-what-you-use
 tool is a program that can be built with the clang libraries in order to
 analyze #includes of source files to find include-what-you-use violations,
 and suggest fixes for them.
 .
 The main goal of include-what-you-use is to remove superfluous #includes.
 It does this both by figuring out what #includes are not actually needed for
 this file (for both .cc and .h files), and replacing #includes with
 forward-declares when possible.
